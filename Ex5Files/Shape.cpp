#include <iostream>
#include "Shape.h"


Shape::Shape(std::string name, std::string type): _name(name),_type(type){
}

std::string Shape::getType() const
{
	return _type;
}

std::string Shape::getName() const
{
	return _name;
}

void Shape::printDetails() const
{
	std::cout << "Name: " << _name << std::endl;
	std::cout << "Type: " << _type << std::endl;
	
}

Shape::~Shape() {

}


