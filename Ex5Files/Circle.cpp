#include <cmath>
#include "Circle.h"

#define PI 3.14
//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

// void Circle::draw(const Canvas& canvas)
// {
// 	canvas.draw_circle(getCenter(), getRadius());
// }

// void Circle::clearDraw(const Canvas& canvas)
// {
// 	canvas.clear_circle(getCenter(), getRadius());
// }

Circle::Circle(Point center, double radius, std::string type, std::string name): Shape(name,type){
	if (radius < 0)
		exit(1);
	_radius = radius;
	_center = center;
}

Circle::~Circle()
{
}

Point Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

double Circle::getArea() const
{
	return PI * pow(_radius, 2);
}

double Circle::getPerimeter() const
{
	return 2 * PI * _radius;
}

void Circle::move(Point other)
{
	_center += other;
}
