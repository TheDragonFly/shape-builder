#include "Arrow.h"

//////////////////////////////////////////////////////////////////////////////
// Canvas and Cimg cannot be used on Linux, 								//
// please make sure to leave it commented if you want test to run on GitLab //
// You can remove comments when you run your exercise locally on Windows .  //
//////////////////////////////////////////////////////////////////////////////

// void Arrow::draw(const Canvas& canvas)
// {
// 	canvas.draw_arrow(_points[0], _points[1]);
// }
// void Arrow::clearDraw(const Canvas& canvas)
// {
// 	canvas.clear_arrow(_points[0], _points[1]);
// }

Arrow::Arrow(Point a, Point b, std::string type, std::string name) : Shape(name,type){
	if (a == b)
		exit(1);
	start = a;
	end = b;

}

Arrow::~Arrow()
{
}

Point Arrow::getSource()const
{
	return start;
}

Point Arrow::getDestination() const
{
	return end;
}

double Arrow::getArea()const
{
	return 0.0;
}

double Arrow::getPerimeter() const
{
	return 0;
}

void Arrow::move(Point other){
	start += other;
	end += other;
}
