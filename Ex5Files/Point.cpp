#include "Point.h"
#include <cmath>

Point::Point():_x(0),_y(0) {

}

Point::Point(double x, double y){
	_x = x;
	_y = y;
}

Point::~Point(){}

Point Point::operator+(const Point& other) const
{
	Point newPoint(_x + other.getX(), _y + other.getY());
	return newPoint;
}

Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	

	return *this;
}

bool Point::operator==(const Point& other) const
{
	return other.getX() == _x && other.getY() == _y ? true : false;
}

void Point::operator=(const Point& other){
	_x = other.getX();
	_y = other.getY();
}



double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(Point& other)
{
	double delta_x = other.getX() - _x;
	double delta_y = other.getY() - _y;

	return sqrt(pow(delta_x,2) + pow(delta_y,2));
}
