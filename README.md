# Ex5 - Shape drawing program
The fifth 'Advanced Programming' course exercise.
In this exercise we implement classes that represents geometric shapes such as:
- Polygon
- Circle
- Rectangle
- Triangle
- Arrow

When we finished implementing the shapes, We'll use a graphic library to draw the shapes on the console

### Ex5Files
Contains base files for Ex5

### Ex5.pdf
Exercise instructions

### .gitlab-ci.yml
Pipeline file for auto-tests

## Submission details

#### Files to submit:

**part 1 - Shapes**
- Point.h
- Point.cpp
- Shape.h
- Shape.cpp
- Arrow.h
- Arrow.cpp
- Circle.h
- Circle.cpp
- Polygon.h
- Polygon.cpp
- Triangle.h
- Triangle.cpp
- Rectangle.h
- Rectangle.cpp
- Menu.h
- Menu.cpp
- Cimg.h
- Canvas.h
- Canvas.cpp
- main.cpp

**part 1 Bonus**
- Quadrangle.h
- Quadrangle.cpp
- BetterCanvas.h
- BetterCanvas.cpp

**Hard Bonus - RAII**
- lightweight_grep.zip
